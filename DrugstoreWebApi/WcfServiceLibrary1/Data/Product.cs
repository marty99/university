﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DrugstoreWebApi.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProducerName { get; set; }
        public int Amount { get; set; }
        public decimal Cost { get; set; }
        public DateTime ArrivedOn { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
