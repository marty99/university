﻿using DrugstoreWebApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        private static bool authenticatedSuccessfully = false;

        public string Authenticate(string key)
        {
            if (key != null && key.Equals("3$231ksMaaFSD23xK"))
            {
                authenticatedSuccessfully = true;
                return "Success";
            }
            else
            {
                return "Invalid key";
            }
        }
       
        #region Get item from specific type by ID
        
        public Client GetClientById(string value)
        {
            //initial response
            var jsonResult = GetClientByIdFromWeb(value).Result;

            //check if there is a successful response
            if (jsonResult == null)
            {
                return null;
            }

            else
            {
                Client client = new Client();
                client = JsonConvert.DeserializeObject<Client>(jsonResult);
                return client;
            }
        }

        private static async Task<string> GetClientByIdFromWeb(string id)
        {
            var url = "https://localhost:44301/api/Clients/" + id;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                HttpResponseMessage response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string strResult = await response.Content.ReadAsStringAsync();

                    return strResult;
                }
                else
                {
                    return null;
                }
            }
        }

        public Order GetOrderById(string value)
        {
            //initial response
            var jsonResult = GetOrderByIdFromWeb(value).Result;

            //check if there is a successful response
            if (jsonResult == null)
            {
                return null;
            }

            else
            {
                Order order = new Order();
                order = JsonConvert.DeserializeObject<Order>(jsonResult);
                order.Client = GetClientById(order.ClientId.ToString());
                order.Product = GetProductById(order.ProductId.ToString());
                return order;
            }
        }

        private static async Task<string> GetOrderByIdFromWeb(string id)
        {
            var url = "https://localhost:44301/api/Orders/" + id;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                HttpResponseMessage response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string strResult = await response.Content.ReadAsStringAsync();

                    return strResult;
                }
                else
                {
                    return null;
                }
            }
        }

        public Product GetProductById(string value)
        {
            //initial response
            var jsonResult = GetProductByIdFromWeb(value).Result;

            //check if there is a successful response
            if (jsonResult == null)
            {
                return null;
            }

            else
            {
                Product product = new Product();
                product = JsonConvert.DeserializeObject<Product>(jsonResult);
                return product;
            }
        }

        private static async Task<string> GetProductByIdFromWeb(string id)
        {
            var url = "https://localhost:44301/api/Products/" + id;// + "&key=" + appKey;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                HttpResponseMessage response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string strResult = await response.Content.ReadAsStringAsync();

                    return strResult;
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion

        #region Get all items from specific type

        public List<Client> GetAllClients()
        {
            //initial response
            var jsonResult = GetAllClientsFromWeb().Result;

            //check if there is a successful response
            if (jsonResult == null)
            {
                return null;
            }

            else
            {
                List<Client> clients = new List<Client>();
                clients = JsonConvert.DeserializeObject<List<Client>>(jsonResult);
                return clients;
            }
        }

        private static async Task<string> GetAllClientsFromWeb()
        {
            var url = "https://localhost:44301/api/Clients";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                HttpResponseMessage response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string strResult = await response.Content.ReadAsStringAsync();

                    return strResult;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<Order> GetAllOrders()
        {
            //initial response
            var jsonResult = GetAllOrdersFromWeb().Result;

            //check if there is a successful response
            if (jsonResult == null)
            {
                return null;
            }

            else
            {
                List<Order> orders = new List<Order>();
                orders = JsonConvert.DeserializeObject<List<Order>>(jsonResult);
                foreach (var item in orders)
                {
                    item.Client = GetClientById(item.ClientId.ToString());
                    item.Product = GetProductById(item.ProductId.ToString());
                }
                return orders;
            }
        }

        private static async Task<string> GetAllOrdersFromWeb()
        {
            var url = "https://localhost:44301/api/Orders";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                HttpResponseMessage response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string strResult = await response.Content.ReadAsStringAsync();

                    return strResult;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<Product> GetAllProducts()
        {
            //initial response
            var jsonResult = GetAllProductsFromWeb().Result;

            //check if there is a successful response
            if (jsonResult == null)
            {
                return null;
            }

            else
            {
                List<Product> products = new List<Product>();
                products = JsonConvert.DeserializeObject<List<Product>>(jsonResult);
                return products;
            }
        }

        private static async Task<string> GetAllProductsFromWeb()
        {
            var url = "https://localhost:44301/api/Products";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                HttpResponseMessage response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string strResult = await response.Content.ReadAsStringAsync();

                    return strResult;
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion

        #region Post item from specific type

        public string PostClient(string fname, string lname, int age, double height, double weight, string date)
        {
            Client client = new Client();
            try
            {
                client.Id = 0;
                if (fname.Length > 50) throw new Exception("First name is too long");
                client.Fname = fname;
                if (lname.Length > 50) throw new Exception("Last name is too long");
                client.Lname = lname;
                client.Age = age;
                if (height < 0 || height > 999.99) throw new Exception("Height must be between 0 and 999.99");
                client.Height = height;
                if (weight < 0 || weight > 999.99) throw new Exception("Weight must be between 0 and 999.99");
                client.Weight = weight;
                client.DateOfBirth = DateTime.Parse(date);
            }
            catch (Exception)
            {
                return "Invalid data";
            }
            return AddClientToWeb(client);
        }

        private static string AddClientToWeb(Client clientToAdd)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Clients/" + apikey);

            RestRequest restRequest = new RestRequest(Method.POST);

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddJsonBody(clientToAdd);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.Created))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        public string PostOrder(int clientId, int productId, int amount, decimal totalCost, string paymentMethod, string datePurchased)
        {
            Order order = new Order();
            try
            {
                order.Id = 0;
                if (GetClientById(clientId.ToString()) == null) throw new Exception("Client does not exist");
                order.ClientId = clientId;
                if (GetProductById(productId.ToString()) == null) throw new Exception("Product does not exist");
                order.ProductId = productId;
                if (amount == 0) throw new Exception("Amount can't be 0");
                order.Amount = amount;
                order.TotalCost = totalCost;
                if (paymentMethod.Length > 25) throw new Exception("Payment method is too long");
                order.PaymentMethod = paymentMethod;
                order.DatePurchased = DateTime.Parse(datePurchased);
            }
            catch (Exception)
            {
                return "Invalid data";
            }
            return AddOrderToWeb(order);
        }

        private static string AddOrderToWeb(Order orderToAdd)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Orders/" + apikey);

            RestRequest restRequest = new RestRequest(Method.POST);

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddJsonBody(orderToAdd);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.Created))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        public string PostProduct(string productName, string producerName, int amount, decimal cost, string arrivedOn, string expirationDate)
        {
            Product product = new Product();
            try
            {
                product.Id = 0;
                if (productName.Length > 50) throw new Exception("Product name is too long");
                product.ProductName = productName;
                if (producerName.Length > 50) throw new Exception("Producer name is too long");
                product.ProducerName = producerName;
                product.Amount = amount;
                product.Cost = cost;
                product.ArrivedOn = DateTime.Parse(arrivedOn);
                product.ExpirationDate = DateTime.Parse(expirationDate);
            }
            catch (Exception)
            {
                return "Invalid data";
            }
            return AddProductToWeb(product);
        }

        private static string AddProductToWeb(Product productToAdd)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Products/" + apikey);

            RestRequest restRequest = new RestRequest(Method.POST);

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddJsonBody(productToAdd);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.Created))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        #endregion

        #region Put/Update item from specific type

        public string PutClient(int id, string fname, string lname, int age, double height, double weight, string date)
        {
            Client client = new Client();
            try
            {
                if (GetClientById(id.ToString()) == null) throw new Exception("Client does not exist, impossible to edit a missing record");
                client.Id = id;
                if (fname.Length > 50) throw new Exception("First name is too long");
                client.Fname = fname;
                if (lname.Length > 50) throw new Exception("Last name is too long");
                client.Lname = lname;
                client.Age = age;
                if (height < 0 || height > 999.99) throw new Exception("Height must be between 0 and 999.99");
                client.Height = height;
                if (weight < 0 || weight > 999.99) throw new Exception("Weight must be between 0 and 999.99");
                client.Weight = weight;
                client.DateOfBirth = DateTime.Parse(date);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return UpdateClientInWeb(client);
        }

        private static string UpdateClientInWeb(Client clientToAdd)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Clients/" + clientToAdd.Id + "&" + apikey);

            RestRequest restRequest = new RestRequest(Method.PUT);

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddJsonBody(clientToAdd);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.NoContent))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        public string PutOrder(int id, int clientId, int productId, int amount, decimal totalCost, string paymentMethod, string datePurchased)
        {
            Order order = new Order();
            try
            {
                if (GetOrderById(id.ToString()) == null) throw new Exception("Order does not exist, impossible to edit a missing record");
                order.Id = id;
                if (GetClientById(clientId.ToString()) == null) throw new Exception("Client does not exist");
                order.ClientId = clientId;
                if (GetProductById(productId.ToString()) == null) throw new Exception("Product does not exist");
                order.ProductId = productId;
                if (amount == 0) throw new Exception("Amount can not be 0");
                order.Amount = amount;
                order.TotalCost = totalCost;
                if (paymentMethod.Length > 25) throw new Exception("Payment method is too long");
                order.PaymentMethod = paymentMethod;
                order.DatePurchased = DateTime.Parse(datePurchased);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return UpdateOrderInWeb(order);
        }

        private static string UpdateOrderInWeb(Order orderToAdd)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Orders/" + orderToAdd.Id + "&" + apikey);

            RestRequest restRequest = new RestRequest(Method.PUT);

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddJsonBody(orderToAdd);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.NoContent))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        public string PutProduct(int id, string productName, string producerName, int amount, decimal cost, string arrivedOn, string expirationDate)
        {
            Product product = new Product();
            try
            {
                if (GetProductById(id.ToString()) == null) throw new Exception("Product does not exist, impossible to edit a missing record");
                product.Id = id;
                if (productName.Length > 50) throw new Exception("Product name is too long");
                product.ProductName = productName;
                if (producerName.Length > 50) throw new Exception("Producer name is too long");
                product.ProducerName = producerName;
                product.Amount = amount;
                product.Cost = cost;
                product.ArrivedOn = DateTime.Parse(arrivedOn);
                product.ExpirationDate = DateTime.Parse(expirationDate);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return UpdateProductInWeb(product);
        }

        private static string UpdateProductInWeb(Product productToAdd)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Products/" + productToAdd.Id + "&" + apikey);

            RestRequest restRequest = new RestRequest(Method.PUT);

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddJsonBody(productToAdd);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.NoContent))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        #endregion

        #region Delete item from specific type

        public string DeleteClient(int id)
        {
            try
            {
                if (GetClientById(id.ToString()) == null) throw new Exception("Client does not exist, impossible to delete a missing record");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return DeleteClientInWeb(id);
        }

        private static string DeleteClientInWeb(int id)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Clients/" + id + "&" + apikey);

            RestRequest restRequest = new RestRequest(Method.DELETE);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        public string DeleteOrder(int id)
        {
            try
            {
                if (GetOrderById(id.ToString()) == null) throw new Exception("Order does not exist, impossible to delete a missing record");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return DeleteOrderInWeb(id);
        }

        private static string DeleteOrderInWeb(int id)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Orders/" + id + "&" + apikey);

            RestRequest restRequest = new RestRequest(Method.DELETE);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        public string DeleteProduct(int id)
        {
            try
            {
                if (GetProductById(id.ToString()) == null) throw new Exception("Product does not exist, impossible to delete a missing record");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return DeleteProductInWeb(id);
        }

        private static string DeleteProductInWeb(int id)
        {
            if (authenticatedSuccessfully == false)
            {
                return "Not authenticated";
            }
            string apikey = "3$231ksMaaFSD23xK";
            RestClient restClient = new RestClient("https://localhost:44301/api/Products/" + id + "&" + apikey);

            RestRequest restRequest = new RestRequest(Method.DELETE);

            IRestResponse restResponse = restClient.Execute(restRequest);

            if (restResponse.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return "Success";
            }
            else
            {
                return "Failed";
            }
        }

        #endregion

        public string GetData2(string value)
        {
            // https://check.bgtoll.bg/check/vignette/plate/BG/A3333MK
            return string.Format("You another value {0} ---", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public static void Do()
        {
            var result = GetClientByIdFromWeb("da10b68dea6a42d58ea8fea66a57b886").Result;

            //TODO parse json here. For example, see http://stackoverflow.com/questions/6620165/how-can-i-parse-json-with-c

            Console.WriteLine(result);
        }

    }
}
