﻿using DrugstoreWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        Client GetClientById(string value);

        [OperationContract]
        Order GetOrderById(string value);

        [OperationContract]
        Product GetProductById(string value);

        [OperationContract]
        List<Client> GetAllClients();

        [OperationContract]
        List<Order> GetAllOrders();

        [OperationContract]
        List<Product> GetAllProducts();

        [OperationContract]
        string PostClient(string fname, string lname, int age, double height, double weight, string date);

        [OperationContract]
        string PostOrder(int clientId, int productId, int amount, decimal totalCost, string paymentMethod, string datePurchased);

        [OperationContract]
        string PostProduct(string productName, string producerName, int amount, decimal cost, string arrivedOn, string expirationDate);
        
        [OperationContract]
        string PutClient(int id, string fname, string lname, int age, double height, double weight, string date);

        [OperationContract]
        string PutOrder(int id, int clientId, int productId, int amount, decimal totalCost, string paymentMethod, string datePurchased);

        [OperationContract]
        string PutProduct(int id, string productName, string producerName, int amount, decimal cost, string arrivedOn, string expirationDate);

        [OperationContract]
        string DeleteClient(int id);

        [OperationContract]
        string DeleteOrder(int id);

        [OperationContract]
        string DeleteProduct(int id);

        [OperationContract]
        string Authenticate(string key);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "WcfServiceLibrary1.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
