﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DrugstoreWebApi.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }

        [Required]
        public int Amount { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalCost { get; set; }
        [StringLength(25)]
        public string PaymentMethod { get; set; }
        [DataType(DataType.Date)]
        public DateTime DatePurchased { get; set; }
    }
}
