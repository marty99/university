﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DrugstoreWebApi.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string ProductName { get; set; }
        [StringLength(50)]
        public string ProducerName { get; set; }
        public int Amount { get; set; }
        [DataType(DataType.Currency)]
        public decimal Cost { get; set; }
        [DataType(DataType.Date)]
        public DateTime ArrivedOn { get; set; }
        [DataType(DataType.Date)]
        public DateTime ExpirationDate { get; set; }
    }
}
