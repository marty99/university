﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DrugstoreWebApi.Models
{
    public class Client
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Fname { get; set; }
        [StringLength(50)]
        public string Lname { get; set; }
        public int Age { get; set; }
        [Range(0, 999.99)]
        public double Height { get; set; }
        [Range(0, 999.99)]
        public double Weight { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
    }
}
