﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DrugstoreWebApi.Models;

namespace DrugstoreWebApi.Data
{
    public class DBContext : DbContext
    {
        public DBContext (DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public DbSet<DrugstoreWebApi.Models.Client> Client { get; set; }

        public DbSet<DrugstoreWebApi.Models.Order> Order { get; set; }

        public DbSet<DrugstoreWebApi.Models.Product> Product { get; set; }
    }
}
