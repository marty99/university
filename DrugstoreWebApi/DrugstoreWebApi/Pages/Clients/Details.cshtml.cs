﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DrugstoreWebApi.Data;
using DrugstoreWebApi.Models;

namespace DrugstoreWebApi.Pages.Clients
{
    public class DetailsModel : PageModel
    {
        private readonly DrugstoreWebApi.Data.DBContext _context;

        public DetailsModel(DrugstoreWebApi.Data.DBContext context)
        {
            _context = context;
        }

        public Client Client { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Client = await _context.Client.FirstOrDefaultAsync(m => m.Id == id);

            if (Client == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
