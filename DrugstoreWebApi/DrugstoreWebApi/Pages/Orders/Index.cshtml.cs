﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DrugstoreWebApi.Data;
using DrugstoreWebApi.Models;

namespace DrugstoreWebApi.Pages.Orders
{
    public class IndexModel : PageModel
    {
        private readonly DrugstoreWebApi.Data.DBContext _context;

        public IndexModel(DrugstoreWebApi.Data.DBContext context)
        {
            _context = context;
        }

        public IList<Order> Order { get;set; }

        public async Task OnGetAsync()
        {
            Order = await _context.Order
                .Include(o => o.Client)
                .Include(o => o.Product).ToListAsync();
        }
    }
}
