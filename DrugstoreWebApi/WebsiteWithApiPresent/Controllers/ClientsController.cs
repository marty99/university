﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceReference1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebsiteWithApiPresent.Controllers
{
    public class ClientsController : Controller
    {
        public ActionResult Index(string search)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            List<Client> clients = new List<Client>();
            foreach (var item in client.GetAllClientsAsync().Result)
            {
                if(search != null)
                {
                    if (item.Fname.Contains(search))
                    {
                        clients.Add(item);
                    }
                }
                else
                {
                    clients.Add(item);
                }
            }
            ViewBag.ClientsList = clients;
            return View();
        }

        public ActionResult Details(int id)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            Client item = client.GetClientByIdAsync(id.ToString()).Result;
            ViewBag.Client = item;
            return View();
        }

        public ActionResult Create(string fname, string lname, string age, string height, string weight, string dateofbirth)
        {
            if (fname != null)
            {
                try
                {
                    ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
                    int age1 = int.Parse(age);
                    double height1 = double.Parse(height);
                    double weight1 = double.Parse(weight);
                    ViewBag.Message = client.PostClientAsync(fname, lname, age1, height1, weight1, dateofbirth).Result;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View();
        }

        public ActionResult Edit(string id, string fname, string lname, string age, string height, string weight, string dateofbirth)
        {
            ViewBag.ItemId = id;
            if (fname != null)
            {
                try
                {
                    ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
                    int id1 = int.Parse(id);
                    int age1 = int.Parse(age);
                    double height1 = double.Parse(height);
                    double weight1 = double.Parse(weight);
                    ViewBag.Message = client.PutClientAsync(id1, fname, lname, age1, height1, weight1, dateofbirth).Result;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            ViewBag.Message = client.DeleteClientAsync(id).Result;
            return View();
        }
    }
}
