﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebsiteWithApiPresent.Controllers
{
    public class AuthenticateController : Controller
    {
        public IActionResult Index(string key)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            if (key != null)
            {
                ViewBag.Message = client.AuthenticateAsync(key).Result;
            }
            return View();
        }
    }
}
