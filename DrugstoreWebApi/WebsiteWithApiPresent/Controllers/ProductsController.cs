﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceReference1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebsiteWithApiPresent.Controllers
{
    public class ProductsController : Controller
    {
        public ActionResult Index(string search)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            List<Product> products = new List<Product>();
            foreach (var item in client.GetAllProductsAsync().Result)
            {
                if (search != null)
                {
                    if (item.ProductName.Contains(search))
                    {
                        products.Add(item);
                    }
                }
                else
                {
                    products.Add(item);
                }
            }
            ViewBag.ProductsList = products;
            return View();
        }

        public ActionResult Details(int id)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            Product item = client.GetProductByIdAsync(id.ToString()).Result;
            ViewBag.Product = item;
            return View();
        }
        public ActionResult Create(string productname, string producername, string amount, string cost, string arrivedon, string expirationdate)
        {
            if (productname != null)
            {
                try
                {
                    ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
                    int amount1 = int.Parse(amount);
                    decimal cost1 = decimal.Parse(cost);
                    ViewBag.Message = client.PostProductAsync(productname, producername, amount1, cost1, arrivedon, expirationdate).Result;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View();
        }

        public ActionResult Edit(string id, string productname, string producername, string amount, string cost, string arrivedon, string expirationdate)
        {
            ViewBag.ItemId = id;
            if (productname != null)
            {
                try
                {
                    ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
                    int id1 = int.Parse(id);
                    int amount1 = int.Parse(amount);
                    decimal cost1 = decimal.Parse(cost);
                    ViewBag.Message = client.PutProductAsync(id1, productname, producername, amount1, cost1, arrivedon, expirationdate).Result;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            ViewBag.Message = client.DeleteProductAsync(id).Result;
            return View();
        }
    }
}
