﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceReference1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebsiteWithApiPresent.Controllers
{
    public class OrdersController : Controller
    {
        public ActionResult Index(string search)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            List<Order> orders = new List<Order>();
            foreach (var item in client.GetAllOrdersAsync().Result)
            {
                if (search != null)
                {
                    if (item.PaymentMethod.Contains(search))
                    {
                        orders.Add(item);
                    }
                }
                else
                {
                    orders.Add(item);
                }
            }
            ViewBag.OrdersList = orders;
            return View();
        }

        public ActionResult Details(int id)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            Order item = client.GetOrderByIdAsync(id.ToString()).Result;
            ViewBag.Order = item;
            return View();
        }

        public ActionResult Create(string clientid, string productid, string amount, string totalcost, string paymentmethod, string datepurchased)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            List<SelectListItem> clientsList = new List<SelectListItem>();
            foreach (var item in client.GetAllClientsAsync().Result)
            {
                clientsList.Add(new SelectListItem(item.Fname, item.Id.ToString()));
            }
            List<SelectListItem> productsList = new List<SelectListItem>();
            foreach (var item in client.GetAllProductsAsync().Result)
            {
                productsList.Add(new SelectListItem(item.ProductName, item.Id.ToString()));
            }
            ViewBag.DropDownClients = clientsList;
            ViewBag.DropDownProducts = productsList;

            if (amount != null)
            {
                try
                {
                    int clientid1 = int.Parse(clientid);
                    int productid1 = int.Parse(productid);
                    int amount1 = int.Parse(amount);
                    decimal totalcost1 = decimal.Parse(totalcost);
                    ViewBag.Message = client.PostOrderAsync(clientid1, productid1, amount1, totalcost1, paymentmethod, datepurchased).Result;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View();
        }

        public ActionResult Edit(string id, string clientid, string productid, string amount, string totalcost, string paymentmethod, string datepurchased)
        {
            ViewBag.ItemId = id;

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            List<SelectListItem> clientsList = new List<SelectListItem>();
            foreach (var item in client.GetAllClientsAsync().Result)
            {
                clientsList.Add(new SelectListItem(item.Fname, item.Id.ToString()));
            }
            List<SelectListItem> productsList = new List<SelectListItem>();
            foreach (var item in client.GetAllProductsAsync().Result)
            {
                productsList.Add(new SelectListItem(item.ProductName, item.Id.ToString()));
            }
            ViewBag.DropDownClients = clientsList;
            ViewBag.DropDownProducts = productsList;


            if (amount != null)
            {
                try
                {
                    int id1 = int.Parse(id);
                    int clientid1 = int.Parse(clientid);
                    int productid1 = int.Parse(productid);
                    int amount1 = int.Parse(amount);
                    decimal totalcost1 = decimal.Parse(totalcost);
                    ViewBag.Message = client.PutOrderAsync(id1, clientid1, productid1, amount1, totalcost1, paymentmethod, datepurchased).Result;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            ViewBag.Message = client.DeleteOrderAsync(id).Result;
            return View();
        }
    }
}
